use std::collections::HashMap;
use std::ffi::CString;

use ash_tray::ash::version::{DeviceV1_0, InstanceV1_1};
use ash_tray::ash::vk;
use ash_tray::vk_helper::*;
use ash_tray::vk_make_version;
use ash_tray::vk_mem;

type VkHelperUser<T> = T;

const DESIRED_WINDOW_WIDTH: u32 = 640;
const DESIRED_WINDOW_HEIGHT: u32 = 480;

fn main() {
    type T = ();

    use std::mem::{align_of, size_of_val};
    use std::ptr::NonNull;
    use std::slice::from_ref;

    use ash_tray::ash::extensions::khr::{RayTracing, Swapchain};
    use ash_tray::ash::util::Align;
    use ash_tray::winit::window;
    use ash_tray::WindowExt;

    let logical_size = ash_tray::winit::dpi::LogicalSize::new(
        DESIRED_WINDOW_WIDTH as f64,
        DESIRED_WINDOW_HEIGHT as f64,
    );
    let event_loop = ash_tray::winit::event_loop::EventLoop::<T>::with_user_event();
    let window = window::WindowBuilder::new()
        .with_title("Ray Trace Ash Tray")
        .with_inner_size(logical_size)
        .with_visible(false)
        .build(&event_loop)
        .map_err(|e| println!("{:?}", e))
        .unwrap();

    let app_name = CString::new("Ray Trace Ash Tray").unwrap();
    let appinfo = vk::ApplicationInfo::builder()
        .application_name(&app_name)
        .application_version(0)
        .engine_name(&app_name)
        .engine_version(0)
        .api_version(vk_make_version(1, 2, 0));

    let mut extension_names_raw = window.get_required_extensions().unwrap();
    extension_names_raw.push(vk::KhrGetPhysicalDeviceProperties2Fn::name().as_ptr());
    let create_info = vk::InstanceCreateInfo::builder()
        .application_info(&appinfo)
        .enabled_extension_names(&extension_names_raw);

    let instance = Instance::<VkHelperUser<T>>::new_simple(&create_info, Default::default())
        .expect("Instance creation error");

    let surface = SurfaceKHR::new(instance.clone(), &window, 0, Default::default())
        .expect("Surface creation error");

    let features = vk::PhysicalDeviceFeatures::builder()
        .shader_clip_distance(true)
        .geometry_shader(true);

    let device_extension_names_raw = [
        Swapchain::name().as_ptr(),
        RayTracing::name().as_ptr(),
        vk::KhrPipelineLibraryFn::name().as_ptr(),
        vk::ExtDescriptorIndexingFn::name().as_ptr(),
        vk::KhrBufferDeviceAddressFn::name().as_ptr(),
        vk::KhrDeferredHostOperationsFn::name().as_ptr(),
        vk::KhrGetMemoryRequirements2Fn::name().as_ptr(),
    ];

    let create_info = [*vk::DeviceQueueCreateInfo::builder()
        .queue_family_index(surface.queue_family_index)
        .queue_priorities(&[1.0])];

    let pnext =
        &mut vk::PhysicalDeviceBufferDeviceAddressFeatures::builder().buffer_device_address(true);

    let create_info = vk::DeviceCreateInfo::builder()
        .queue_create_infos(&create_info)
        .enabled_extension_names(&device_extension_names_raw)
        .enabled_features(&features)
        .push_next(pnext);

    let device = Device::new(
        instance.clone(),
        surface.physical_device,
        &create_info,
        Default::default(),
    )
    .unwrap();
    let allocator = Allocator::new(
        instance.clone(),
        surface.physical_device,
        device.clone(),
        &mut vk_mem::AllocatorCreateInfo {
            flags: vk_mem::AllocatorCreateFlags::BUFFER_DEVICE_ADDRESS,
            ..Default::default()
        },
        Default::default(),
    )
    .unwrap();
    let present_queue = Queue::new(
        device.clone(),
        surface.queue_family_index,
        0,
        Default::default(),
    );

    let create_info = vk::CommandPoolCreateInfo::builder()
        .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
        .queue_family_index(surface.queue_family_index);

    let command_pool = CommandPool::new(device.clone(), &create_info, Default::default()).unwrap();

    let ray_tracing_properties = &mut vk::PhysicalDeviceRayTracingPropertiesKHR::default();
    unsafe {
        instance.get_physical_device_properties2(
            surface.physical_device,
            &mut vk::PhysicalDeviceProperties2::builder().push_next(ray_tracing_properties),
        )
    };

    let acceleration_create_geometry_info =
        &vk::AccelerationStructureCreateGeometryTypeInfoKHR::builder()
            .geometry_type(vk::GeometryTypeKHR::TRIANGLES)
            .max_primitive_count(128)
            .index_type(vk::IndexType::UINT32)
            .max_vertex_count(8)
            .vertex_format(vk::Format::R32G32B32A32_SFLOAT);

    let acceleration_info = &vk::AccelerationStructureCreateInfoKHR::builder()
        .ty(vk::AccelerationStructureTypeKHR::BOTTOM_LEVEL)
        .flags(vk::BuildAccelerationStructureFlagsKHR::PREFER_FAST_TRACE)
        .geometry_infos(from_ref(acceleration_create_geometry_info));

    let bottom_level_as = AccelerationStructure::new(
        allocator.clone(),
        acceleration_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::GpuOnly,
            ..Default::default()
        },
        (),
    )
    .unwrap();

    let bottom_scratch = Buffer::new_acceleration_structure_scratch(
        allocator.clone(),
        vk::AccelerationStructureMemoryRequirementsTypeKHR::BUILD_SCRATCH,
        bottom_level_as.clone(),
        &vk::BufferCreateInfo::default(),
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::GpuOnly,
            ..Default::default()
        },
        (),
    )
    .unwrap();

    let vertex_data = &[1.0f32, 1.0, 0.0, -1.0, 1.0, 0.0, 0.0, -1.0, 0.0];
    let index_data = &[0u32, 1, 2];

    let size_of_data = size_of_val(vertex_data);
    let create_info = vk::BufferCreateInfo::builder()
        .size(size_of_data as _)
        .usage(vk::BufferUsageFlags::VERTEX_BUFFER);

    let vertex_buffer = Buffer::new(
        allocator.clone(),
        &create_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::CpuToGpu,
            flags: vk_mem::AllocationCreateFlags::MAPPED,
            ..Default::default()
        },
        Default::default(),
    )
    .unwrap();

    let allocation_info = vertex_buffer
        .allocator
        .clone()
        .borrow_mut()
        .get_allocation_info(&vertex_buffer.allocation)
        .unwrap();
    let (mapped_data, mapped_size) = (
        allocation_info.get_mapped_data(),
        allocation_info.get_size(),
    );
    assert!(size_of_data <= mapped_size);
    let mut vertex_align = unsafe {
        Align::new(
            NonNull::new_unchecked(mapped_data).cast().as_mut(),
            align_of::<T>() as u64,
            mapped_size as _,
        )
    };

    vertex_align.copy_from_slice(vertex_data);
    vertex_buffer.flush(0, 0).unwrap();

    let size_of_data = size_of_val(index_data);
    let create_info = vk::BufferCreateInfo::builder()
        .size(size_of_data as _)
        .usage(vk::BufferUsageFlags::INDEX_BUFFER);

    let index_buffer = Buffer::new(
        allocator.clone(),
        &create_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::CpuToGpu,
            flags: vk_mem::AllocationCreateFlags::MAPPED,
            ..Default::default()
        },
        Default::default(),
    )
    .unwrap();

    let allocation_info = index_buffer
        .allocator
        .borrow_mut()
        .get_allocation_info(&index_buffer.allocation)
        .unwrap();
    let (mapped_data, mapped_size) = (
        allocation_info.get_mapped_data(),
        allocation_info.get_size(),
    );
    assert!(size_of_data <= mapped_size);
    let mut index_align = unsafe {
        Align::new(
            NonNull::new_unchecked(mapped_data).cast().as_mut(),
            align_of::<u32>() as u64,
            mapped_size as _,
        )
    };

    index_align.copy_from_slice(index_data);
    index_buffer.flush(0, 0).unwrap();

    let mut geometry = vk::AccelerationStructureGeometryDataKHR::default();
    geometry.triangles = *vk::AccelerationStructureGeometryTrianglesDataKHR::builder()
        .vertex_format(vk::Format::R32G32B32_SFLOAT)
        .vertex_stride(3 * 4)
        .index_type(vk::IndexType::UINT32);
    let acceleration_geometry =
        vk::AccelerationStructureGeometryKHR::builder().flags(vk::GeometryFlagsKHR::OPAQUE);

    let acceleration_build_geometry_info = vk::AccelerationStructureBuildGeometryInfoKHR::builder()
        .ty(vk::AccelerationStructureTypeKHR::BOTTOM_LEVEL)
        .flags(vk::BuildAccelerationStructureFlagsKHR::PREFER_FAST_TRACE);

    let acceleration_build_offset_info =
        vk::AccelerationStructureBuildOffsetInfoKHR::builder().primitive_count(1);
    let acceleration_build_offsets = &[*acceleration_build_offset_info];

    let create_info =
        vk::CommandBufferAllocateInfo::builder().level(vk::CommandBufferLevel::PRIMARY);

    let command_buffer = CommandBuffer::new(
        command_pool.clone(),
        Fence::new(device.clone(), &Default::default(), Default::default())
            .expect("Create fence failed."),
        create_info,
        Default::default(),
    )
    .unwrap();

    let begin_info =
        vk::CommandBufferBeginInfo::builder().flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

    unsafe { device.begin_command_buffer(**command_buffer, &begin_info) }
        .expect("Begin command buffer");

    command_buffer.build_acceleration_structure(
        None,
        bottom_level_as,
        &mut [(
            acceleration_geometry,
            AccelerationStructureGeometryContents::Triangle {
                vertex_data: vertex_buffer,
                index_data: Some(index_buffer),
                transform_data: None,
            },
        )],
        bottom_scratch,
        acceleration_build_geometry_info,
        acceleration_build_offsets,
    );

    let acceleration_create_geometry_info =
        &vk::AccelerationStructureCreateGeometryTypeInfoKHR::builder()
            .geometry_type(vk::GeometryTypeKHR::TRIANGLES)
            .max_primitive_count(128)
            .index_type(vk::IndexType::UINT32)
            .max_vertex_count(8)
            .vertex_format(vk::Format::R32G32B32A32_SFLOAT);

    let acceleration_info = &vk::AccelerationStructureCreateInfoKHR::builder()
        .ty(vk::AccelerationStructureTypeKHR::TOP_LEVEL)
        .flags(vk::BuildAccelerationStructureFlagsKHR::PREFER_FAST_TRACE)
        .geometry_infos(from_ref(acceleration_create_geometry_info));

    let top_level_as = AccelerationStructure::new(
        allocator.clone(),
        acceleration_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::GpuOnly,
            ..Default::default()
        },
        (),
    )
    .unwrap();

    let top_scratch = Buffer::new_acceleration_structure_scratch(
        allocator.clone(),
        vk::AccelerationStructureMemoryRequirementsTypeKHR::BUILD_SCRATCH,
        top_level_as.clone(),
        &vk::BufferCreateInfo::default(),
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::GpuOnly,
            ..Default::default()
        },
        (),
    )
    .unwrap();

    let geometry_data_data = &[vk::AccelerationStructureInstanceKHR {
        transform: vk::TransformMatrixKHR {
            matrix: [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
        },
        instance_custom_index_and_mask: 0xff,
        instance_shader_binding_table_record_offset_and_flags: 0x1,
        acceleration_structure_reference: 0,
    }];

    let size_of_data = size_of_val(geometry_data_data);
    let create_info = vk::BufferCreateInfo::builder()
        .size(size_of_data as _)
        .usage(vk::BufferUsageFlags::SHADER_DEVICE_ADDRESS);

    let geometry_data_buffer = Buffer::new(
        allocator.clone(),
        &create_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::CpuToGpu,
            flags: vk_mem::AllocationCreateFlags::MAPPED,
            ..Default::default()
        },
        Default::default(),
    )
    .unwrap();

    let allocation_info = geometry_data_buffer
        .allocator
        .clone()
        .borrow_mut()
        .get_allocation_info(&geometry_data_buffer.allocation)
        .unwrap();
    let (mapped_data, mapped_size) = (
        allocation_info.get_mapped_data(),
        allocation_info.get_size(),
    );
    assert!(size_of_data <= mapped_size);
    let mut geometry_data_align = unsafe {
        Align::new(
            NonNull::new_unchecked(mapped_data).cast().as_mut(),
            align_of::<T>() as u64,
            mapped_size as _,
        )
    };

    geometry_data_align.copy_from_slice(geometry_data_data);
    geometry_data_buffer.flush(0, 0).unwrap();

    let mut geometry = vk::AccelerationStructureGeometryDataKHR::default();
    geometry.instances = *vk::AccelerationStructureGeometryInstancesDataKHR::builder();

    let acceleration_geometry =
        vk::AccelerationStructureGeometryKHR::builder().flags(vk::GeometryFlagsKHR::OPAQUE);
    let acceleration_build_geometry_info = vk::AccelerationStructureBuildGeometryInfoKHR::builder()
        .ty(vk::AccelerationStructureTypeKHR::TOP_LEVEL)
        .flags(vk::BuildAccelerationStructureFlagsKHR::PREFER_FAST_TRACE);
    let acceleration_build_offset_info =
        vk::AccelerationStructureBuildOffsetInfoKHR::builder().primitive_count(1);
    let acceleration_build_offsets = &[*acceleration_build_offset_info];

    let ray_tracing_fn = &RayTracing::new(&**instance, &**device);
    command_buffer.build_acceleration_structure(
        None,
        top_level_as.clone(),
        &mut [(
            acceleration_geometry,
            AccelerationStructureGeometryContents::Instance(
                geometry_data_buffer,
                vec![top_level_as.clone()],
            ),
        )],
        top_scratch,
        acceleration_build_geometry_info,
        acceleration_build_offsets,
    );

    unsafe { device.end_command_buffer(**command_buffer) }.unwrap();

    command_buffer
        .queue_submit(present_queue.clone(), vec![], vk::SubmitInfo::builder())
        .unwrap();
    command_buffer.wait_and_destroy(std::u64::MAX).unwrap();

    let extent = vk::Extent3D {
        width: DESIRED_WINDOW_WIDTH,
        height: DESIRED_WINDOW_HEIGHT,
        depth: 1,
    };
    let image_info = vk::ImageCreateInfo::builder()
        .image_type(vk::ImageType::TYPE_2D)
        .format(surface.surface_format.format)
        .extent(extent)
        .mip_levels(1)
        .array_layers(1)
        .samples(vk::SampleCountFlags::TYPE_1)
        .usage(vk::ImageUsageFlags::STORAGE);

    let offscreen_buffer = Image::new(
        allocator.clone(),
        &image_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::GpuOnly,
            ..Default::default()
        },
        Default::default(),
    )
    .unwrap();

    let image_view_info = vk::ImageViewCreateInfo::builder()
        .view_type(vk::ImageViewType::TYPE_2D)
        .format(surface.surface_format.format)
        .subresource_range(
            *vk::ImageSubresourceRange::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .level_count(1)
                .layer_count(1),
        )
        .components(
            *vk::ComponentMapping::builder()
                .r(vk::ComponentSwizzle::R)
                .g(vk::ComponentSwizzle::G)
                .b(vk::ComponentSwizzle::B)
                .a(vk::ComponentSwizzle::A),
        );

    let offscreen_buffer_view = ImageView::new(
        offscreen_buffer.clone(),
        image_view_info,
        Default::default(),
    )
    .unwrap();

    let acceleration_structure_layout_binding = vk::DescriptorSetLayoutBinding::builder()
        .descriptor_type(vk::DescriptorType::ACCELERATION_STRUCTURE_KHR)
        .descriptor_count(1)
        .stage_flags(vk::ShaderStageFlags::RAYGEN_KHR);
    let storage_image_layout_binding = vk::DescriptorSetLayoutBinding::builder()
        .binding(1)
        .descriptor_type(vk::DescriptorType::STORAGE_IMAGE)
        .descriptor_count(1)
        .stage_flags(vk::ShaderStageFlags::RAYGEN_KHR);

    let descriptor_set_layout = DescriptorSetLayout::new(
        device.clone(),
        &[
            *acceleration_structure_layout_binding,
            *storage_image_layout_binding,
        ],
        vk::DescriptorSetLayoutCreateInfo::builder(),
        Default::default(),
    )
    .unwrap();

    let pool_sizes = &[
        *vk::DescriptorPoolSize::builder()
            .ty(vk::DescriptorType::ACCELERATION_STRUCTURE_KHR)
            .descriptor_count(1),
        *vk::DescriptorPoolSize::builder()
            .ty(vk::DescriptorType::STORAGE_IMAGE)
            .descriptor_count(1),
    ];
    let descriptor_pool_info = &vk::DescriptorPoolCreateInfo::builder()
        .max_sets(1)
        .pool_sizes(pool_sizes);

    let descriptor_pool =
        DescriptorPool::new(device.clone(), descriptor_pool_info, Default::default()).unwrap();

    let descriptor_sets = DescriptorSet::new(
        &descriptor_pool,
        from_ref(&descriptor_set_layout),
        vk::DescriptorSetAllocateInfo::builder(),
        Default::default(),
        vec![],
    )
    .unwrap();

    descriptor_sets[0]
        .write_acceleration_structures(vec![top_level_as], vk::WriteDescriptorSet::builder());
    descriptor_sets[0].write_images(
        vec![(
            offscreen_buffer_view,
            vk::DescriptorImageInfo::builder().image_layout(vk::ImageLayout::GENERAL),
        )],
        vk::WriteDescriptorSet::builder().dst_binding(1),
    );

    let pipeline_layout = PipelineLayout::new(
        device.clone(),
        vec![descriptor_set_layout],
        vk::PipelineLayoutCreateInfo::builder(),
        Default::default(),
    )
    .unwrap();

    let mut shaders = HashMap::new();
    shaders.insert(
        vk::ShaderStageFlags::RAYGEN_KHR,
        ShaderModule::new_simple(
            device.clone(),
            std::include_bytes!("../shaders/ray-generation.spv"),
            Default::default(),
        )
        .unwrap(),
    );
    shaders.insert(
        vk::ShaderStageFlags::CLOSEST_HIT_KHR,
        ShaderModule::new_simple(
            device.clone(),
            std::include_bytes!("../shaders/ray-closest-hit.spv"),
            Default::default(),
        )
        .unwrap(),
    );
    shaders.insert(
        vk::ShaderStageFlags::ANY_HIT_KHR,
        ShaderModule::new_simple(
            device.clone(),
            std::include_bytes!("../shaders/ray-miss.spv"),
            Default::default(),
        )
        .unwrap(),
    );

    let shader_entry_name = std::ffi::CStr::from_bytes_with_nul(b"main\0").unwrap();
    let shader_stage_create_infos = [
        *vk::PipelineShaderStageCreateInfo::builder()
            .stage(vk::ShaderStageFlags::RAYGEN_KHR)
            .name(&shader_entry_name),
        *vk::PipelineShaderStageCreateInfo::builder()
            .stage(vk::ShaderStageFlags::CLOSEST_HIT_KHR)
            .name(&shader_entry_name),
        *vk::PipelineShaderStageCreateInfo::builder()
            .stage(vk::ShaderStageFlags::ANY_HIT_KHR)
            .name(&shader_entry_name),
    ];

    let shader_groups = &[
        *vk::RayTracingShaderGroupCreateInfoKHR::builder()
            .general_shader(0)
            .closest_hit_shader(vk::SHADER_UNUSED_KHR)
            .any_hit_shader(vk::SHADER_UNUSED_KHR)
            .intersection_shader(vk::SHADER_UNUSED_KHR),
        *vk::RayTracingShaderGroupCreateInfoKHR::builder()
            .general_shader(vk::SHADER_UNUSED_KHR)
            .closest_hit_shader(1)
            .any_hit_shader(vk::SHADER_UNUSED_KHR)
            .intersection_shader(vk::SHADER_UNUSED_KHR),
        *vk::RayTracingShaderGroupCreateInfoKHR::builder()
            .general_shader(2)
            .closest_hit_shader(vk::SHADER_UNUSED_KHR)
            .any_hit_shader(vk::SHADER_UNUSED_KHR)
            .intersection_shader(vk::SHADER_UNUSED_KHR),
    ];
    let pipeline_info = vk::RayTracingPipelineCreateInfoKHR::builder()
        .groups(shader_groups)
        .max_recursion_depth(1)
        .libraries(Default::default());
    let pipeline = Pipeline::new_ray_tracing(
        pipeline_layout,
        shaders,
        &shader_stage_create_infos,
        pipeline_info,
        Default::default(),
    )
    .unwrap();

    const GROUP_NUMBER: u32 = 3;
    let shader_binding_table_size =
        (GROUP_NUMBER * ray_tracing_properties.shader_group_handle_size) as vk::DeviceSize;

    let buffer_info = &vk::BufferCreateInfo::builder()
        .size(shader_binding_table_size)
        .usage(vk::BufferUsageFlags::TRANSFER_SRC | vk::BufferUsageFlags::RAY_TRACING_KHR);

    let shader_binding_table = Buffer::new(
        allocator,
        buffer_info,
        &vk_mem::AllocationCreateInfo {
            usage: vk_mem::MemoryUsage::CpuToGpu,
            flags: vk_mem::AllocationCreateFlags::MAPPED,
            ..Default::default()
        },
        Default::default(),
    )
    .unwrap();

    let ptr = shader_binding_table
        .allocator
        .borrow_mut()
        .get_allocation_info(&shader_binding_table.allocation)
        .unwrap()
        .get_mapped_data();

    unsafe {
        ray_tracing_fn.get_ray_tracing_shader_group_handles(
            **pipeline,
            0,
            GROUP_NUMBER,
            std::slice::from_raw_parts_mut::<'_, u8>(ptr, shader_binding_table_size as _),
        )
    }
    .unwrap();

    shader_binding_table.flush(0, 0).unwrap();

    let present_modes = unsafe {
        surface
            .surface_loader
            .get_physical_device_surface_present_modes(surface.physical_device, **surface)
    }
    .unwrap();
    let present_mode = present_modes
        .iter()
        .cloned()
        .find(|&mode| mode == vk::PresentModeKHR::MAILBOX)
        .unwrap_or(vk::PresentModeKHR::FIFO);

    const DESIRED_IMAGE_COUNT: u32 = 3;
    let create_info = vk::SwapchainCreateInfoKHR::builder()
        .min_image_count(DESIRED_IMAGE_COUNT)
        .image_format(surface.surface_format.format)
        .image_extent(vk::Extent2D {
            width: DESIRED_WINDOW_WIDTH,
            height: DESIRED_WINDOW_HEIGHT,
        })
        .image_array_layers(1)
        .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT | vk::ImageUsageFlags::TRANSFER_DST)
        .pre_transform(vk::SurfaceTransformFlagsKHR::IDENTITY)
        .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
        .present_mode(present_mode)
        .clipped(true);

    let swapchain = SwapchainKHR::new(
        device.clone(),
        surface.clone(),
        create_info,
        Default::default(),
        Default::default(),
        vec![],
    )
    .unwrap();

    /*
        let present_image_views: Vec<ImageView<VkHelperUser<T>>> = swapchain
            .present_images
            .iter()
            .map(|image| {
                let create_info = vk::ImageViewCreateInfo::builder()
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(surface.surface_format.format)
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    });

                ImageView::new(image.clone(), create_info, Default::default()).unwrap()
            })
            .collect();
    */
    let copy_region = vk::ImageCopy::builder()
        .src_subresource(
            *vk::ImageSubresourceLayers::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .layer_count(1),
        )
        .dst_subresource(
            *vk::ImageSubresourceLayers::builder()
                .aspect_mask(vk::ImageAspectFlags::COLOR)
                .layer_count(1),
        )
        .extent(extent);

    let subresource_range = vk::ImageSubresourceRange::builder()
        .aspect_mask(vk::ImageAspectFlags::COLOR)
        .level_count(1)
        .layer_count(1);

    let command_buffers = swapchain
        .present_images
        .iter()
        .map(|swapchain_image| {
            let ray_gen_sbt = vk::StridedBufferRegionKHR::builder().size(shader_binding_table_size);
            let ray_miss_sbt = vk::StridedBufferRegionKHR::builder()
                .offset((2 * ray_tracing_properties.shader_group_handle_size) as vk::DeviceSize)
                .size(shader_binding_table_size);
            let ray_hit_sbt = vk::StridedBufferRegionKHR::builder()
                .offset((1 * ray_tracing_properties.shader_group_handle_size) as vk::DeviceSize)
                .size(shader_binding_table_size);
            let ray_callable_sbt = vk::StridedBufferRegionKHR::builder();

            let create_info =
                vk::CommandBufferAllocateInfo::builder().level(vk::CommandBufferLevel::PRIMARY);

            let command_buffer = CommandBuffer::new(
                command_pool.clone(),
                Fence::new(device.clone(), &Default::default(), Default::default())
                    .expect("Create fence failed."),
                create_info,
                Default::default(),
            )
            .unwrap();

            let begin_info = vk::CommandBufferBeginInfo::builder();

            unsafe { device.begin_command_buffer(**command_buffer, &begin_info) }
                .expect("Begin command buffer");

            let image_memory_barrier = vk::ImageMemoryBarrier::builder()
                .dst_access_mask(vk::AccessFlags::SHADER_WRITE)
                .old_layout(vk::ImageLayout::UNDEFINED)
                .new_layout(vk::ImageLayout::GENERAL)
                .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .subresource_range(*subresource_range);

            command_buffer.pipeline_barrier(
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                &[],
                vec![],
                vec![(offscreen_buffer.clone(), image_memory_barrier)],
            );

            command_buffer.bind_pipeline(pipeline.clone());
            command_buffer.bind_descriptor_sets(0, descriptor_sets.clone(), &[]);
            command_buffer.trace_rays(
                vec![(shader_binding_table.clone(), ray_gen_sbt)],
                vec![(shader_binding_table.clone(), ray_miss_sbt)],
                vec![(shader_binding_table.clone(), ray_hit_sbt)],
                vec![(None, ray_callable_sbt)],
                DESIRED_WINDOW_WIDTH,
                DESIRED_WINDOW_HEIGHT,
                1,
            );

            let image_memory_barrier = vk::ImageMemoryBarrier::builder()
                .dst_access_mask(vk::AccessFlags::TRANSFER_WRITE)
                .old_layout(vk::ImageLayout::UNDEFINED)
                .new_layout(vk::ImageLayout::TRANSFER_DST_OPTIMAL)
                .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .subresource_range(*subresource_range);

            command_buffer.pipeline_barrier(
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                &[],
                vec![],
                vec![(offscreen_buffer.clone(), image_memory_barrier)],
            );

            let image_memory_barrier = vk::ImageMemoryBarrier::builder()
                .src_access_mask(vk::AccessFlags::SHADER_WRITE)
                .dst_access_mask(vk::AccessFlags::TRANSFER_READ)
                .old_layout(vk::ImageLayout::GENERAL)
                .new_layout(vk::ImageLayout::TRANSFER_SRC_OPTIMAL)
                .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
                .subresource_range(*subresource_range);

            command_buffer.pipeline_barrier(
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::PipelineStageFlags::ALL_COMMANDS,
                vk::DependencyFlags::empty(),
                &[],
                vec![],
                vec![(swapchain_image.clone(), image_memory_barrier)],
            );

            command_buffer.copy_image(
                offscreen_buffer.clone(),
                vk::ImageLayout::TRANSFER_SRC_OPTIMAL,
                swapchain_image.clone(),
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                from_ref(&copy_region),
            );

            unsafe { device.end_command_buffer(**command_buffer) }.expect("End command buffer");

            command_buffer
        })
        .collect::<Vec<_>>();

    let present_complete_semaphore =
        Semaphore::new(device.clone(), &Default::default(), Default::default())
            .expect("First Semaphore creation error");
    let rendering_complete_semaphore =
        Semaphore::new(device, &Default::default(), Default::default())
            .expect("Second Semaphore creation error");

    window.set_visible(true);

    event_loop.run(move |event, _target, control| {
        use ash_tray::winit::{event, event_loop::ControlFlow};
        match event {
            event::Event::WindowEvent {
                event: event::WindowEvent::CloseRequested,
                ..
            } => *control = ControlFlow::Exit,
            event::Event::MainEventsCleared => window.request_redraw(),
            event::Event::RedrawRequested(_) => {
                use ash_tray::vk_helper::Error::VkAcquireNextImages;
                match swapchain.acquire_next_image(
                    50_000_000u64, /* 0.05s, 20hz */
                    **present_complete_semaphore,
                    vk::Fence::null(),
                ) {
                    Ok((present_index, _)) => {
                        command_buffers[present_index as usize]
                            .queue_submit(present_queue.clone(), vec![], vk::SubmitInfo::builder())
                            .unwrap();
                        let present_info =
                            vk::PresentInfoKHR::builder().image_indices(from_ref(&present_index));
                        swapchain
                            .queue_present(
                                present_queue.clone(),
                                vec![rendering_complete_semaphore.clone()],
                                present_info,
                            )
                            .unwrap();
                        command_buffers[present_index as usize]
                            .fence_wait(std::u64::MAX)
                            .unwrap();
                    }
                    Err(VkAcquireNextImages {
                        source: vk::Result::ERROR_OUT_OF_DATE_KHR,
                    }) => {}
                    Err(VkAcquireNextImages {
                        source: vk::Result::TIMEOUT,
                    }) => panic!("Cheako said \"It's time to close,\" Good Night."),
                    Err(VkAcquireNextImages { source: e }) => Err(e).unwrap(),
                    Err(_) => unreachable!(),
                };
            }
            _ => {}
        }
    });
}
